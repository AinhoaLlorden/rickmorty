import { clear } from "./common";


let fetchRickMortyRandom = () => {
  clear();
  const div = document.getElementById("rickmortyrandom");
    div.innerHTML = '<img class="character-item__img" src = "../assets/loader.gif">';
    
  const idRandom = Math.round(Math.random() * (826 - 1) + 1);
  console.log(idRandom);
  fetch("https://rickandmortyapi.com/api/character/" + idRandom + "")
    .then((response) => response.json())
    .then((myJson) => {
      console.log(myJson);
      const characterDetail = myJson;

      getCharacterList(characterDetail);
    })
    .catch((error) => {
      console.error(error);
    });
};

const getCharacterList = (characterDetail) => {
  
  // document.getElementById("rickmortydex").innerHTML = "";
  // document.getElementById("rickmortyfind").innerHTML = "";
  document.getElementById("input-find").value = "";

  const div = document.getElementById("rickmortyrandom");
  div.innerHTML = "";

  const ul = document.createElement("ul");
  div.appendChild(ul);
  ul.classList.add("index-container");
  ul.innerHTML += `<li class="character-item-random"><div><img src = ${characterDetail.image}><p class="character-item-random__text">${characterDetail.name}<p> is ${characterDetail.status} and lives in ${characterDetail.origin.name}</p></div></li>`;
};

export { fetchRickMortyRandom };
