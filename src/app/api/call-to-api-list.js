import {fetchRickMortyListRuta, clear} from "./common";



let fetchRickMortyList = () => {
  clear();
  //misma funcionalidad, una línea:
  //document.getElementById("rickmortydex").innerHTML = '<img class="character-item__img" src = "../assets/loader.gif">';

   const div = document.getElementById("rickmortydex");
    div.innerHTML = '<img class="character-item__img" src = "../assets/loader.gif">';

  document.getElementById("input-find").value = "";

  fetchRickMortyListRuta("https://rickandmortyapi.com/api/character");
  
};



export { fetchRickMortyList};
