var prev = "";
var next = "";

let buttonprev = () => {
  console.log(prev);
  fetchRickMortyListRuta(prev);
};

let buttonnext = () => {
  console.log(next);
  fetchRickMortyListRuta(next);
};

import {fetchRickMortyListRuta, clear} from "./common";

const characterFind = () => {
  clear();
  const div = document.getElementById("rickmortyfind");
    div.innerHTML = '<img class="character-item__img" src = "../assets/loader.gif">';
  // document.getElementById("rickmortydex").innerHTML = "";
  // document.getElementById("rickmortyrandom").innerHTML = "";

  const inputFind = document.getElementById("input-find");

  let addClick = () => {
    console.log(inputFind.value);
  };

  inputFind.addEventListener("input", addClick);
  fetchRickMortyDetailFind(inputFind.value);
};

let fetchRickMortyDetailFind = (filtro) => {
  console.log(filtro);

  fetch("https://rickandmortyapi.com/api/character/?name=" + filtro + "")
    .then((response) => response.json())
    .then((myJsonFind) => {
      console.log(myJsonFind);
      if (myJsonFind.error == "There is nothing here") {
        document.getElementById("rickmortyfind").innerHTML =
          '<img class="character-item__random" src="./assets/error-rickmorty.png">';
      }else {
        const characterList = myJsonFind.results;
        prev = myJsonFind.info.prev;
        next = myJsonFind.info.next;
        getCharacterList(characterList);
      }
    })
    .catch((error) => {
      console.error(error);
    });
};

// let fetchRickMortyListRuta = (ruta) => {
//   console.log(ruta);
//   fetch(ruta)
//     .then((response) => response.json())
//     .then((myJson) => {
//       console.log(myJson);
//       const characterList = myJson.results;
//       prev = myJson.info.prev;
//       next = myJson.info.next;
//       getCharacterList(characterList);
//     })
//     .catch((error) => {
//       console.error(error);
//     });
// };



const getCharacterList = (characterList) => {
  // clear();
  const div = document.getElementById("rickmortyfind");
  div.textContent = "";
  const ul = document.createElement("ul");
  div.appendChild(ul);
  ul.classList.add("index-container");

  characterList.forEach((element) => {
    ul.innerHTML += `<li class="character-item"><div><img class="character-item__img" src = ${element.image}><p class="character-item__text">${element.name}</p></div></li>`;
  });

  const buttons = document.getElementById("rickmortyfind");
  buttons.innerHTML += `<div><button type="button" id="prev" class="prev-next__button">Anterior</button><button type="button" id="next" class="prev-next__button">Siguiente</button></div>`;
  if (prev == null){
    document.getElementById("prev").hidden=true;
  }else {
    document.getElementById("prev").addEventListener("click", buttonprev);
  }

  if (next == null){
    document.getElementById("next").hidden=true;
  }else {
    document.getElementById("next").addEventListener("click", buttonnext);
  }
};

export { fetchRickMortyDetailFind };
export { characterFind };
