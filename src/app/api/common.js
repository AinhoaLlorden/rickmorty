var prev = "";
var next = "";

let buttonprev = () => {
  console.log(prev);
  fetchRickMortyListRuta(prev);
};

let buttonnext = () => {
  console.log(next);
  fetchRickMortyListRuta(next);
};




let fetchRickMortyListRuta = (ruta) => {
clear();
  fetch(ruta)
    .then((response) => response.json())
    .then((myJson) => {
      console.log(myJson);
      const characterList = myJson.results;
      prev = myJson.info.prev;
      next = myJson.info.next;
      getCharacterList(characterList);
    })
    .catch((error) => {
      console.error(error);
    });
};

const getCharacterList = (characterList) => {
    const div = document.getElementById("rickmortydex");
    div.innerHTML = "";
  
    const ul = document.createElement("ul");
    div.appendChild(ul);
    ul.classList.add("index-container");
  
    characterList.forEach((element) => {
      ul.innerHTML += `<li class="character-item"><div><img class="character-item__img" src = ${element.image}><p class="character-item__text">${element.name}</p></div></li>`;
    });
    const buttons = document.getElementById("rickmortydex");
  
    buttons.innerHTML += `<div><button type="button" id="prev" class="prev-next__button">Anterior</button><button type="button" id="next" class="prev-next__button">Siguiente</button></div>`;
    
      if (prev == null){
        document.getElementById("prev").hidden=true;
      }else {
        document.getElementById("prev").addEventListener("click", buttonprev);
      }
  
      if (next == null){
        document.getElementById("next").hidden=true;
      }else {
        document.getElementById("next").addEventListener("click", buttonnext);
      }
    
  };

  let clear = () => {
    document.getElementById("rickmortydex").innerHTML = "";
    document.getElementById("rickmortyfind").innerHTML = "";
    document.getElementById("rickmortyrandom").innerHTML = "";
  }

  export { fetchRickMortyListRuta, buttonnext, buttonprev, clear };