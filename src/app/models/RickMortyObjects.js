
class CharacterClass {
  constructor(name, image) {
   
    this.name = name;
    this.image = image;
  
  }


  getCharacterName() {
    return `${this.name}`;
  }

  getCharacterImg() {
    return this.image;
  }


}

class CharacterDetailClass extends CharacterClass {
  constructor(name, image, status, species) {
    super(name, image);
    this.status = status;
    this.species = species;
  }

  getCharacterStatus() {
    return this.status;
  }
  getCharacterSpecies() {
    return this.species;
  }
}

class CharacterRandom extends CharacterDetailClass {
  constructor(name, image, gender) {
    super(name, image, status, species);

  }

  getCharacterGender() {
    return this.gender;
  }
}

export { CharacterClass, CharacterDetailClass, CharacterRandom };
