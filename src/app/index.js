import "./styles/styles.scss";
import "bootstrap";

import { fetchRickMortyList } from "./api/call-to-api-list";
import {
  fetchRickMortyDetailFind,
  characterFind,
} from "./api/call-to-api-detail";
import { fetchRickMortyRandom } from "./api/call-to-api-random";

window.onload = function () {
  addListeners();
};

function addListeners() {
  document.getElementById("find-all").addEventListener("click", fetchRickMortyList);
  document
    .getElementById("input-find")
    .addEventListener("input", characterFind);
  document
    .getElementById("find-random")
    .addEventListener("click", fetchRickMortyRandom);
}
